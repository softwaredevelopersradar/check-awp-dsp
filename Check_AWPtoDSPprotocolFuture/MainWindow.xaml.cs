﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Protocols;

namespace Check_AWPtoDSPprotocolFuture
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AWPtoDSPprotocolFuture dsp = new AWPtoDSPprotocolFuture();

        public MainWindow()
        {
            InitializeComponent();

            textBox.Text = "192.168.0.193";
            //textBox.Text = "192.168.0.111";
            //textBox.Text = "127.0.0.1";

            dsp.IsConnected += Dsp_IsConnected;

            dsp.IsRead += Dsp_IsRead;
            dsp.IsWrite += Dsp_IsWrite;     

            //166
            dsp.NumberOfSatellitesUpdate += dsp_NumberOfSatellitesUpdate;
            //170
            dsp.ShaperModeUpdate += Dsp_ShaperModeUpdate;
            //171
            dsp.ShaperVoltageUpdate += dsp_ShaperVoltageUpdate;
            //172
            dsp.ShaperPowerUpdate += dsp_ShaperPowerUpdate;
            //173
            dsp.ShaperTemperatureUpdate += dsp_ShaperTemperatureUpdate;
            //174
            dsp.ShaperAmperageUpdate += dsp_ShaperAmperageUpdate;

        }

       

        private void Dsp_IsRead(bool isRead)
        {
            Console.WriteLine("Read");
        }

        private void Dsp_IsWrite(bool isWrite)
        {
            Console.WriteLine("Write");
        }

        void dsp_NumberOfSatellitesUpdate(NumberOfSatellitesUpdateEvent answer)
        {
            //Console.Beep();
            Console.WriteLine(answer.NumberOfSatellites);
        }

        void Dsp_ShaperModeUpdate(ShaperModeUpdateEvent answer)
        {
           // Console.Beep();
        }

        void dsp_ShaperVoltageUpdate(ShaperVoltageUpdateEvent answer)
        {
           // Console.Beep();
        }

        void dsp_ShaperPowerUpdate(ShaperPowerUpdateEvent answer)
        {
           // Console.Beep();
        }

        void dsp_ShaperTemperatureUpdate(ShaperTemperatureUpdateEvent answer)
        {
            //Console.Beep();
        }

        void dsp_ShaperAmperageUpdate(ShaperAmperageUpdateEvent answer)
        {
            //Console.Beep();
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void Dsp_IsConnected(bool isConnected)
        {
            DispatchIfNecessary(() =>
            {
                textBox.Background = new SolidColorBrush((isConnected) ? Colors.Green : Colors.Red);
            });
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            string IP = textBox.Text;
            int port = 10001;

            DispatchIfNecessary(async () =>
            {
                await dsp.ConnectToBearingDSP(IP, port);
            });

            //await dsp.ConnectToBearingDSP(IP, port);
        }

        private void ButtonDisconnect_Click(object sender, RoutedEventArgs e)
        {
            dsp.DisconnectFromBearingDSP();
        }

        private async void Button66_Click(object sender, RoutedEventArgs e)
        {
            var asnwer = await dsp.ResetGNSS();
        }

        private async void Button67_Click(object sender, RoutedEventArgs e)
        {
            int intSync = 60;
            var asnwer = await dsp.SetGpsSyncInterval(intSync);
        }

        private async void Button68_Click(object sender, RoutedEventArgs e)
        {
            short ErrorCoord = 20;
            var asnwer = await dsp.SetGpsPositionError(ErrorCoord);
        }

        private void Button69_Click(object sender, RoutedEventArgs e)
        {
            dsp.Test69();
        }

        private async void Button70_Click(object sender, RoutedEventArgs e)
        {
            var asnwer = await dsp.FHSGetState();
        }

        private async void Button71_Click(object sender, RoutedEventArgs e)
        {
            var asnwer = await dsp.FHSGetParam();
        }

        private async void Button72_Click(object sender, RoutedEventArgs e)
        {
            byte SwitchAntenna = 0;
            var asnwer = await dsp.FHSGetParam(SwitchAntenna);
        }

        private async void Button73_Click(object sender, RoutedEventArgs e)
        {
            var asnwer = await dsp.SwitchOffRadiat();
        }

        private async void Button74_Click(object sender, RoutedEventArgs e)
        {
            FRSJammingSetting[] fRSJammingSettings = new FRSJammingSetting[0];
            var asnwer = await dsp.SwitchOnFWS(fRSJammingSettings);
        }

        private async void Button75_Click(object sender, RoutedEventArgs e)
        {
           FhssJammingSetting[] fhssJammingSettings = new FhssJammingSetting[0];
           var asnwer = await dsp.SwitchOnFHSS(fhssJammingSettings);
        }

        private async void Button76_Click(object sender, RoutedEventArgs e)
        {
            var asnwer = await dsp.ResetFHS();
        }

        private async void Button77_Click(object sender, RoutedEventArgs e)
        {
            short ShaperStateUpdateInterval = 50;
            var asnwer = await dsp.SetShaperStateUpdateInterval(ShaperStateUpdateInterval);
        }

        private async void Button78_Click(object sender, RoutedEventArgs e)
        {
            short ShaperSettingsUpdateInterval = 200;
            var asnwer = await dsp.SetShaperSettingsUpdateInterval(ShaperSettingsUpdateInterval);
        }

        private async void Button79_Click(object sender, RoutedEventArgs e)
        {
            byte[] Bands = new byte[100];
            for (int i = 0; i < Bands.Count(); i++)
            {
                Bands[i] = (byte)i;
            }
            var answer = await dsp.GetBandsAdaptiveThreshold(Bands);
        }
    }
}
